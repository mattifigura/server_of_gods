#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include "Misc.h"
#include "PerlinNoise.h"

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <sys/socket.h>
#endif

#include <map>
#include <unordered_map>
#include <list>
#include <queue>
#include <mutex>

using std::queue;

// server implementation of terrain
// we generate terrain, do lighting calculations etc and send this data to the clients based on their position
#define CHUNK_SIZE 16
#define ONE_OVER_CHUNK_SIZE 1.f / CHUNK_SIZE
#define BLOCK_SIZE 10.0f
#define ONE_OVER_BLOCK_SIZE 1.f / BLOCK_SIZE
#define COMPRESSED_CHUNK_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * 3
#define CHUNK_GROUP_SIZE 512

// TerrainBlock structure
#pragma pack(1)
struct TerrainBlock {
	GLuint BlockType : 24;
	GLuint RotX : 1;
	GLuint RotY : 1;
	GLuint RotZ : 1;
	GLuint Multi : 1;
	GLuint Ignored : 1;
	GLuint XX : 1; // lower left
	GLuint XY : 1; // lower right
	GLuint YX : 1; // upper left
	GLuint YY : 1; // upper right
	GLuint LightRed : 4;
	GLuint LightGreen : 4;
	GLuint LightBlue : 4;
	GLuint SkyLight : 4;
	GLuint Solid : 1;
	GLuint Water : 1;
	GLuint Leaked : 1;
	GLuint Half : 1;

	GLuint padding : 11;

	bool operator==(const TerrainBlock& b) { return (BlockType == b.BlockType); }
	bool operator!=(const TerrainBlock& b) { return !(*this == b); }
	void operator=(const TerrainBlock& b)
	{
		BlockType = b.BlockType;
		RotX = b.RotX;
		RotY = b.RotY;
		RotZ = b.RotZ;
		Multi = b.Multi;
		Ignored = b.Ignored;
		XX = b.XX; XY = b.XY; YX = b.YX; YY = b.YY;
		LightRed = b.LightRed;
		LightGreen = b.LightGreen;
		LightBlue = b.LightBlue;
		SkyLight = b.SkyLight;
		Solid = b.Solid;
		Water = b.Water;
		Leaked = b.Leaked;
		Half = b.Half;
	}

	TerrainBlock() { BlockType = 0; RotX = 0; RotY = 0; RotZ = 0; Multi = 0; Ignored = 1; XX = 0; XY = 0; YX = 0; YY = 0; LightRed = 0; LightGreen = 0; LightBlue = 0; SkyLight = 0; Solid = 0; Water = 0; Leaked = 0; Half = 0; }
	TerrainBlock(GLuint type, GLuint rotx = 0, GLuint roty = 0, GLuint rotz = 0, GLuint multi = 0, GLuint ignored = 0, GLuint xx = 0, GLuint xy = 0, GLuint yx = 0, GLuint yy = 0,
		GLuint red = 0, GLuint green = 0, GLuint blue = 0, GLuint sky = 0, GLuint s = 2, GLuint water = 0, GLuint leaked = 0, GLuint half = 0)
	{
		BlockType = type; RotX = rotx; RotY = roty; RotZ = rotz; Multi = multi; Ignored = ignored; XX = xx; XY = xy; YX = yx; YY = yy; LightRed = red; LightGreen = green; LightBlue = blue; SkyLight = sky; Solid = s < 2 ? s : type > 0 ? 1 : 0; Water = water; Leaked = leaked; Half = half;
	}
	TerrainBlock(const TerrainBlock& b)
	{
		BlockType = b.BlockType;
		RotX = b.RotX;
		RotY = b.RotY;
		RotZ = b.RotZ;
		Multi = b.Multi;
		Ignored = b.Ignored;
		XX = b.XX; XY = b.XY; YX = b.YX; YY = b.YY;
		LightRed = b.LightRed;
		LightGreen = b.LightGreen;
		LightBlue = b.LightBlue;
		SkyLight = b.SkyLight;
		Solid = b.Solid;
		Water = b.Water;
		Leaked = b.Leaked;
		Half = b.Half;
	}
};
#pragma pack()

#define CHUNK_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(TerrainBlock)
#define CHUNK_WATER_BUFFER_SIZE CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE * sizeof(unsigned char)

class Item;
struct ItemClass;

// structures for biome generation
struct height_func {
	double smoothx;
	double smoothy;
	double amplify;
	int mode;
};

struct layer {
	GLuint block;
	int depth;
	bool smooth;
};

struct ore_spawn {
	GLuint index;
	int density;
	int max;
	int upper;
	int lower;
};

struct object_gen {
	GLuint index;
	int depth;
	int chance;
};

struct Biome {
	int index;
	string name;
	vector<height_func> funcs;
	map<int, layer> layers;
	vector<ore_spawn> ores;
	vector<object_gen> misc;

	void operator=(const Biome& other)
	{
		index = other.index;
		name = other.name;

		funcs.clear();
		layers.clear();
		ores.clear();
		misc.clear();

		for (GLuint i = 0; i < other.funcs.size(); i++) {
			funcs.push_back(other.funcs[i]);
		}
		std::map<int, layer>::const_iterator it = other.layers.begin();
		while (it != other.layers.end())
		{
			layers.emplace(it->first, it->second);

			it++;
		}
		for (GLuint i = 0; i < other.ores.size(); i++) {
			ores.push_back(other.ores[i]);
		}
		for (GLuint i = 0; i < other.misc.size(); i++) {
			misc.push_back(other.misc[i]);
		}
	}

	bool operator==(const Biome& other) { return (index == other.index); }
};

// block details struct, for interactables and blocks with inventories etc
struct BlockDetails {
	// count represents how many clients have used this block, if it reaches zero then close the container
	int count;

	// this is a pointer to an inventory for this block, if it is not null then this block is a chest-type block
	vector<Item*>* inventory;
};

// as opposed to the client, terrainchunks here are simple structs as they don't need to exist as much more
#pragma pack(1)
struct TerrainChunk {
	//vector3di chunk_position;
	TerrainBlock terrain[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	unsigned char currentWater[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	unsigned char newWater[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	map<vector3di, std::list<vector3di>*> multiBlockLists;
	map<vector3di, BlockDetails> blockDetails;
	bool bGenerated;
	bool bNeedsGeneration;
	bool bWaterSettled;

	TerrainChunk() : bGenerated(false), bNeedsGeneration(false), bWaterSettled(false) { memset(terrain, 0, sizeof(terrain)); memset(currentWater, 0, sizeof(currentWater)); memset(newWater, 0, sizeof(newWater)); }
};
#pragma pack()

// forward declaration of sockaddr_in
struct sockaddr_in;
class Server;

typedef std::map<vector3di, TerrainChunk*> TerrainMap;

class TerrainManager {
	public:
		TerrainManager(Server* server, string name, long seed, string biomesfile);
		~TerrainManager();

		bool LoadBiomes();

		void Update();

		TerrainBlock GetBlock(vector3di cpos, vector3di pos);
		TerrainBlock GetBlock(vector3df pos);

		GLfloat GetHeightBelow(vector3df pos) const;
		GLfloat GetDistanceAbove(vector3df pos) const;

		Biome GetBiome(const vector3df& pos);
		Biome GetBiome(const vector3di& chunkPos, const vector3di& blockPos);
		void SetBiome(const vector3di& chunkPos, const vector3di& blockPos, const Biome& biome);
		vector<int>* GetSector(const vector2di& pos, vector2di& out);

		void GetCornerChunkPosition(const vector3df& pos, vector3di& out);
		void SendChunk(vector3di pos, SOCKET& socket, sockaddr_in& client, GLuint counter);
		void SendWater(vector3di pos, SOCKET& socket, sockaddr_in& client, GLuint counter);

		void ProcessChangeBlock(unsigned char* buf, int& size);
		bool ProcessInteraction(unsigned char* buf);
		void ProcessBlockInventoryUpdate(unsigned char* buf, int rlen);

		void SaveChunk(const vector3di& chunkPos);
		void SaveWater(const vector3di& chunkPos);
		void SaveMultiblockLists(const vector3di& chunkPos);
		void SaveBlockDetails(const vector3di& chunkPos);
		void SaveTerrain();
		void Shutdown();

		void GenerateTerrain(const vector3di& pos, bool reset = false);

		bool GetPositionInChunk(vector3df& pos, TerrainChunk*& chunk, vector3di& out, vector3di& chunkPos) const;
		void ConvertToChunkBlockPosition(const vector3df& pos, vector3di& blockPos, vector3di& chunkPos) const;

		void CleanUpVacantChunks();

	protected:

	private:
		Server*										m_pServer;
		TerrainMap									m_Terrain;
		std::map<vector2di, std::vector<GLfloat>>	m_TerrainNoise;
		std::map<vector2di, std::vector<int>>		m_TerrainBiomes;
		long										m_TerrainSeed;
		PerlinNoise									m_Noise;
		string										m_WorldName;
		string										m_BiomesFile;
		vector<Biome>								m_Biomes;
		int											m_SectorSize;
		int											m_ChunkLengthPerSector;
		map<vector2di, vector<int>>					m_BiomeMap;
		std::mutex									m_Mutex;

		void GenerateNoise(const vector2di& pos, std::vector<GLfloat>& out, std::vector<int>& b);

		void _GenerateTree(TerrainChunk* c, GLint x, GLint y, GLint z);

		std::list<vector3di>* GetMultiBlockList(vector3di block, vector3di& chunk, TerrainChunk*& pChunk);
		void RemoveFromMultiBlockList(map<vector3di, std::list<vector3di>*>* pMap, const vector3di& pos);
		void TerrainBlockToBuffer(TerrainBlock& in, vector3di& blockPos, vector3di& chunkPos, unsigned char* buf, int& start);

		void SendInventory(int clientid, TerrainChunk* pChunk, const vector3di& blockPos);
		vector3df ChunkBlockPosToVector3df(const vector3di& blockPos, const vector3di& chunkPos);
		TerrainBlock _GetBlock(const vector3di& blockPos, const vector3di& chunkPos) const;
		TerrainChunk* GetChunk(const vector3di& pos) const;
		void PositionCorrect(vector3di& blockPos, vector3di& chunkPos);

		TerrainBlock* _BlockAt(vector3di& blockPos, vector3di& chunkPos, TerrainChunk** out);

		// lighting
		struct LightNode {
			//vector3df pos;
			vector3di blockPos;
			vector3di chunkPos;
			GLuint red;
			GLuint green;
			GLuint blue;

			LightNode(vector3di bp, vector3di cp, GLuint r, GLuint g, GLuint b) : blockPos(bp), chunkPos(cp), red(r), green(g), blue(b) {}
		};

		struct SunlightNode {
			//vector3df pos;
			vector3di blockPos;
			vector3di chunkPos;
			GLuint val;

			SunlightNode(vector3di bp, vector3di cp, GLuint v) : blockPos(bp), chunkPos(cp), val(v) {}
		};

		struct LightUpdateNode {
			vector3di blockPos;
			vector3di chunkPos;
			TerrainBlock old;
		};

		vector<LightUpdateNode>	m_LightUpdates;
		queue<LightNode>	m_BlockLightCreation;
		queue<LightNode>	m_BlockLightDeletion;
		queue<SunlightNode>	m_SunLightCreation;
		queue<SunlightNode>	m_SunLightDeletion;

		TerrainChunk* SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue);
		void RemoveBlockLight(const vector3di& blockPos, const vector3di& chunkPos, const TerrainBlock& block);
		TerrainChunk* _SetBlockLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint red, GLuint green, GLuint blue);
		void UpdateLightCreation();
		void UpdateLightDeletion();
		TerrainChunk* SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val);
		void RemoveSunLight(const vector3di& blockPos, const vector3di& chunkPos, const TerrainBlock& block);
		TerrainChunk* _SetSunLight(const vector3di& blockPos, const vector3di& chunkPos, GLuint val);
		void UpdateSunlightCreation();
		void UpdateSunlightDeletion();

		void WriteDataFile(string filename, const vector3di& chunkPos, unsigned char* data, int size);
		bool ReadDataFile(string filename, const vector3di& chunkPos, unsigned char* data, int& size);

		string GroupFileName(const vector3di& groupPos, string path = "chunkdata");
		vector3di GetGroupPos(const vector3di& chunkPos);

		void InventoryToData(vector<Item*>* inv, vector<unsigned char>& out);
		int GetItemCount(vector<Item*>* inv);

		bool LoadChunk(const vector3di& chunkPos);
		bool LoadWater(const vector3di& chunkPos);
		bool LoadMultiblockLists(const vector3di& chunkPos);
		bool LoadBlockDetails(const vector3di& chunkPos);

		void GetPositionInSector(const vector2di& pos, const vector2di& sectorPos, vector2di& out) const;

		float GetChunkPercentageSpace(const vector3di& chunkPos);

		void SetWater(TerrainChunk* chunk, const vector3di& pos, GLfloat water);
		void SetNewWater(TerrainChunk* chunk, const vector3di& pos, GLfloat water);
		GLfloat GetWater(TerrainChunk* chunk, const vector3di& pos) const;
		GLfloat GetNewWater(TerrainChunk* chunk, const vector3di& pos) const;

		static bool m_bSaving;
		static bool m_bShuttingDown;

		bool IsCave(const vector3di& chunkPos, int x, int y, int z);

		void DeleteChunk(TerrainChunk* chunk);
};

#endif
