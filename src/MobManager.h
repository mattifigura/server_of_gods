#ifndef _MOB_MANAGER_H_
#define _MOB_MANAGER_H_

#define MAX_MOBS_PER_CLIENT 10

#include "Misc.h"

class Server;
class Client;
class TerrainManager;

//----------------------------------------------------------------------------\\
// Mobs can be customised by the player through the editing of the XML file.  \\
// Each mob entry will have a number of things listed, varying depending on   \\
// what type of mob it is. This should allow for easy mob modding into the    \\
// game - simply copy an existing mob, tweak the values and the player will   \\
// be able to start finding the mob in game (based on spawn conditions).      \\
//----------------------------------------------------------------------------\\

enum MobAggression {
	friendly,
	neutral,
	hostile
};

enum MobCombatClass {
	melee,
	ranged
};

struct MobClass {
	string								strName;
	int									iHealth;
	short								sDamage;
	float								fRange;
	MobAggression						Aggression;
	int									index;
	MobCombatClass						Style;
	int									iProjectileIndex;

	MobClass()
	{
		strName = "Mob";
		iHealth = 0;
		sDamage = 0;
		fRange = 85.f;
		Aggression = neutral;
		index = -1;
		Style = melee;
		iProjectileIndex = 0;
	}

	MobClass(MobClass* other)
	{
		strName = other->strName;
		iHealth = other->iHealth;
		sDamage = other->sDamage;
		Aggression = other->Aggression;
		index = other->index;
		Style = other->Style;
		iProjectileIndex = other->iProjectileIndex;
	}
};

struct Mob {
	MobData data;
	const MobClass* type;
	int target;
	float dir;
	float grav;
	float knockbackdir;
	float knockbackspeed;
	int zerohealthcount;
	int hitcount;
	int	rangecount;
	bool lunge;
};

class MobManager {
	public:
		MobManager(Server* server, TerrainManager* terrain);
		~MobManager();

		static void ThreadTask(MobManager* m);

		static bool	LoadMobs(string file, string type);
		static void	UnloadMobs();
		static bool	ReloadMobs(string file, string type);
		static const MobClass* GetMobClass(string name);
		static const MobClass* GetMobClass(int id);

		void ClientMeleeAttacking(Client* client);
		void KnockBack(int index, float dir, float speed);
		void KnockBack(Mob* mob, float dir, float speed);

		const vector<Mob*>* GetMobs() const;
		bool Dodge(Mob* mob, float angle);

	protected:
		static std::map<string, MobClass*>		ListOfMobs;
		static std::vector<MobClass*>			IntListOfMobs;
		static std::map<string, vector<int>>	BiomeMobMap;

	private:
		Server*					m_pServer;
		TerrainManager*			m_pTerrain;
		int						m_MaxMobs;
		vector<Mob*>			m_Mobs;
		map<int, vector<Mob*>>	m_MobsPerClient;

		void Update();
		int GetFreeMob();
		void MoveMob(Mob* mob);
		vector<int>* MobList(string biome);
		bool ReassignMob(GLuint mob, std::vector<Client*>& clients);

		bool MoveCheck(const vector3df& pos, GLfloat& fMoveX, GLfloat& fMoveY);
};

#endif