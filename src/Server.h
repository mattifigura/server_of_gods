#ifndef _SERVER_H_
#define _SERVER_H_

#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#else
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#include <string>
#include <thread>
#include <sstream>
#include <mutex>
#include "Terrain.h"
#include "MobManager.h"
#include "zlib.h"
#include "Misc.h"
#include "Item.h"

#define BUFFER_SIZE 512
#define HEADER_SIZE 6
#define ACK_MSG_COUNT 20
#define DAY_BEGIN 0.35f
#define DAY_END 2 - DAY_BEGIN

// in an attempt to keep the code as similar between platforms as possible, define stuff here so I don't have to change the code much
#ifdef __linux
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#endif

/*
	First bit structure:

	0 - server bit, has the message arrived from the server?
	1 - acknowledge bit, is this message an acknowledgment for another?
	2 - request bit, is this message a request for data?
*/

// message header struct
#pragma pack(1)
struct MessageHeader {
	unsigned char info;
	unsigned char message;
	unsigned int counter;

	MessageHeader() : info(0), message(0), counter(0) {}
	MessageHeader(const MessageHeader& other) : info(other.info), message(other.message), counter(other.counter) {}
	bool operator==(const MessageHeader& other) { return (message == other.message && counter == other.counter); }
	MessageHeader& operator=(const MessageHeader& other) { info = other.info; message = other.message; counter = other.counter; return *this; }

	static unsigned char* ToBuffer(MessageHeader& header) { return reinterpret_cast<unsigned char*>(&header); }
	static MessageHeader& FromBuffer(unsigned char* buffer) { return *reinterpret_cast<MessageHeader*>(buffer); }
};
#pragma pack()

// ack message struct
struct AckMsg {
	bool isfree;
	unsigned char* buf;
	int len;
	int count;
	MessageHeader header;

	bool Compare(const MessageHeader& other)
	{
		return (header == other);
	}
};

// item info
struct InventoryItem {
	const ItemClass* ic;
	string name;
	short amount;
};

// client structure
struct Client {
	// details about this client
	int clientid;
	bool newlyconnected;
	PlayerData data;
	string name;
	bool statsreceived;
	Stats stats;
	bool deathtimerstarted;
	int hitcount;
	float hpregen;
	float mpregen;
	float hpregenspeed;
	float mpregenspeed;
	int timeout;
	float attack;
	vector3df spawn;
	int defence;
	int biome;
	
	// hotbar info
	struct {
		unsigned char* data;
		int len;
		bool newdata;
		vector<InventoryItem> items;
	} hotbar, equipment;

	// ack messages
	AckMsg ackmsgs[ACK_MSG_COUNT];
	AckMsg* FindFreeAckMsg()
	{
		for (GLuint i = 0; i < ACK_MSG_COUNT; i++) {
			if (ackmsgs[i].isfree)
				return &ackmsgs[i];
		}

		return NULL;
	}
	void AddAckMsg(const MessageHeader& header, unsigned char* buffer, int len)
	{
		AckMsg* msg = FindFreeAckMsg();
		if (msg)
		{
			memset(msg->buf, 0, BUFFER_SIZE);
			memcpy(msg->buf, &buffer[1], len - 1);
			msg->len = len - 1;
			msg->isfree = false;
			msg->count = 6;
			msg->header = header;
		}
	}

	// socket information
	sockaddr_in sinfo;
};

// projectile structure - this holds projectile information on the server
struct Projectile {
	ProjectileData data;
	short damage;
	float gravity;
	short knockback;
	int clientid;
	float life;
};

// itemfilter structure
struct ItemFilter {
	ItemType type_filter;
	SubType subtype_filter;

	ItemFilter() { type_filter = NO_TYPE; subtype_filter = NO_SUBTYPE; }
	ItemFilter(ItemType type, SubType subtype = NO_SUBTYPE) { type_filter = type; subtype_filter = subtype; }
	void operator=(const ItemFilter& other) { type_filter = other.type_filter; subtype_filter = other.subtype_filter; }
	bool operator==(const ItemFilter& other) { return (type_filter == other.type_filter && subtype_filter == other.subtype_filter); }
	bool operator!=(const ItemFilter& other) { return !(*this == other); }
};

class Server {
	public:
		Server();

		void Run();
		bool IsRunning();

		bool GetListOfClients(std::vector<Client*>& out);
		Client** GetWholeListOfClients() const;
		int GetMaxClientCount();

		void ProcessCommand(const string& in);

		bool SendPacket(int clientid, unsigned char type, unsigned char* buf, int size, bool ack = false, bool req = false);
		bool SendPacketToAllClients(unsigned char type, unsigned char* buf, int size, bool ack = false, bool req = false);
		bool SendAck(int clientid, unsigned char* buf, int size = 0);
		ItemData& CreateItem(string name, vector3df pos, short amount = 1);

		GLuint NextTxCounter(int client, int message);
		GLuint GetTxCounter(int client, int message) const;
		bool SetTxCounter(int client, int message, GLuint counter);
		void ResetTxCounter(int client, int message);
		GLuint GetRxCounter(int client, int message) const;
		bool SetRxCounter(int client, int message, GLuint counter);
		void ResetRxCounter(int client, int message);
		void ResetAllCounters(int client);

		void CreateProjectile(string name, vector3df pos, vector2df rotation, short damage, short knockback, Client* c = NULL);
		void CreateProjectile(const ItemClass* ammo, vector3df pos, vector2df rotation, short damage, short knockback, Client* c = NULL);
		void CreateProjectile(string name, vector3df pos, vector3df target, short damage, short knockback, Client* c = NULL);
		void CreateProjectile(const ItemClass* ammo, vector3df pos, vector3df target, short damage, short knockback, Client* c = NULL);

		// client control functions
		void MoveClient(int client, const vector3df& pos);
		void HitClient(int client, short damage, const vector3df& pos);
		void SetClientHealth(int client, unsigned short health);
		void SetClientEnergy(int client, unsigned short energy);

	protected:

	private:
		SOCKET					m_Socket;
		sockaddr_in				m_Server, m_Other;
		short					m_PortNumber;
		bool					m_Initialised;
		static bool				m_Running;

		TerrainManager*			m_pTerrain;
		std::thread*			m_pTerrainThread;

		MobManager*				m_pMobMan;
		std::thread*			m_pMobManThread;

		//vector<Client*>		m_Clients;
		int						m_MaxClients;
		Client**				m_Clients;
		std::thread*			m_pClientThread;
		std::thread*			m_pWorldThread;
		std::thread*			m_pServerTickThread;
		GLfloat					m_TimeOfDay;
		std::mutex				m_Mutex;
		std::list<Projectile>	m_Projectiles;

		vector<ItemData>		m_Items;
		vector<vector<GLuint>>	m_RxCounters;
		vector<vector<GLuint>>	m_TxCounters;

		string					m_ServerName;
		string					m_WorldName;
		long					m_WorldSeed;
		string					m_BiomesFile;

		void WriteServerConfig();
		void ReadConfigSetting(string name, string value);

		int GetFreeClientID();

		void SendToAllClients(char* buf, int size);
		void SendToAllOtherClients(char* buf, int size, int exclude);
		void AddAckMsgToAllClients(unsigned char* buf, int size);
		void AddAckMsgToAllOtherClients(unsigned char* buf, int size, int exclude);

		void ProcessData(unsigned char* buf, int rlen);
		static void ClientThread(Server* s);
		static void WorldThread(Server* s);
		static void ServerTickThread(Server* s);
		static void ReviveClient(Server* s, Client* c);

		Client* GetClientFromSockAddrInfo(const sockaddr_in& s);
		Client* GetClientFromID(int id);
		Client* GetClientFromName(const string& name);

		bool FindItem(const ItemData& data, GLuint& out);
		void AcknowledgeMessage(const MessageHeader& other, const sockaddr_in& s);

		void ClientUsingItem(Client* c, double delta);
		void AddProjectile(Projectile p);
		void SendStats(Client* c);

		bool FilterCheck(const ItemClass* ic, const ItemFilter& filter);
		InventoryItem* FindItem(const ItemFilter& filter, vector<InventoryItem>* inv);

		void ProcessClientAction(Client* c, int type);
};

#endif
