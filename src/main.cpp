#include "Server.h"
#include <iostream>

// The "Server of Gods" - the server side for the now abandoned Pyre of Gods game

void ServerThread(Server& server)
{
	// run the server
	server.Run();
}

void CommandThread(Server* server)
{
	// wait a few seconds before accepting commands
	std::this_thread::sleep_for(std::chrono::milliseconds(3000));
	
	// wait for input
	while (true)
	{
		// wait for a command
		string in;
		std::getline(std::cin, in);

		// send the command to the server object
		server->ProcessCommand(in);

		// print a >
		std::cout << "> ";
	}
}

int main(int argc, char** argv)
{
	// create the server
	Server server;

	// create the command thread
	std::thread* commandThread = new std::thread(CommandThread, &server);
	commandThread->detach();

	// run the server
	server.Run();

	// delete the command thread
	delete commandThread;

	// we have finished
	return 1;
}
