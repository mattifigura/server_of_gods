#ifndef _MISC_H_
#define _MISC_H_

#include "Triangle.h"
#include "MessageDefinitions.h"
#include <string>
#include <map>
#include <vector>
#include <thread>
#include <functional>
#include <iostream>
#include <sys/stat.h>
#include <locale>
#include <fstream>

#ifdef __linux__
typedef int SOCKET;
#endif

using std::string;
using std::map;
using std::vector;

#define _out std::cout << "\r"
#define _end std::endl << "> ";

enum {
	server_packet,
	new_connection,
	disconnect,
	player_data,
	player_hotbar_data,
	player_equipment_data,
	player_stat_data,
	player_action,
	terrain_block_data,
	terrain_water_data,
	change_block,
	create_item,
	destroy_item,
	chat_message,
	block_interaction,
	block_inventory,
	use_item,
	mob_updates,
	new_projectile,

	max_message_number
};

enum {
	sp_server_update,
	sp_reset_counter,
	sp_kick_player,
	sp_give_item,
	sp_move_player,
	sp_knockback,
	sp_new_biome
};

enum {
	player_rolled
};

template<typename T> static void SetBit(T &n, int pos, int val)
{
	if (val == 1)
	{
		n |= 1 << pos;
	}
	else if (val == 0)
	{
		n &= ~(1 << pos);
	}
}

template<typename T> static bool GetBit(const T n, int pos)
{
	return ((n & 1 << pos) == (1 << pos) ? 1 : 0);
}

static void IntToByte(const int& in, unsigned char* out)
{
	union {
		int i;
		unsigned char b[4];
	} conv;
	conv.i = in;
	memcpy(out, conv.b, 4);
}

static void ByteToInt(const unsigned char* in, int& out)
{
	union {
		int i;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.i;
}

static void UIntToByte(const unsigned int& in, unsigned char* out)
{
	union {
		unsigned int i;
		unsigned char b[4];
	} conv;
	conv.i = in;
	memcpy(out, conv.b, 4);
}

static void ByteToUInt(const unsigned char* in, unsigned int& out)
{
	union {
		unsigned int i;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.i;
}

static void ShortToByte(const short& in, unsigned char* out)
{
	union {
		short s;
		unsigned char b[2];
	} conv;
	conv.s = in;
	memcpy(out, conv.b, 2);
}

static void ByteToShort(const unsigned char* in, short& out)
{
	union {
		short s;
		unsigned char b[2];
	} conv;
	memcpy(conv.b, in, 2);
	out = conv.s;
}

static void UShortToByte(const unsigned short& in, unsigned char* out)
{
	union {
		unsigned short s;
		unsigned char b[2];
	} conv;
	conv.s = in;
	memcpy(out, conv.b, 2);
}

static void ByteToUShort(const unsigned char* in, unsigned short& out)
{
	union {
		unsigned short s;
		unsigned char b[2];
	} conv;
	memcpy(conv.b, in, 2);
	out = conv.s;
}

static void FloatToByte(const float& in, unsigned char* out)
{
	union {
		float f;
		unsigned char b[4];
	} conv;
	conv.f = in;
	memcpy(out, conv.b, 4);
}

static void ByteToFloat(const unsigned char* in, float& out)
{
	union {
		float f;
		unsigned char b[4];
	} conv;
	memcpy(conv.b, in, 4);
	out = conv.f;
}

struct StringCompare {
	bool operator()(const string& a, const string& b) const
	{
		const int al = a.length();
		const int bl = b.length();
		if (al != bl)
			return al < bl;

		return a < b;
	}
};

template <class function, class... arguments> static void Timer(unsigned int time, bool newthread, function&& fn, arguments&&... args)
{
	std::function<typename std::result_of<function(arguments...)>::type()> execute(std::bind(std::forward<function>(fn), std::forward<arguments>(args)...));

	if (newthread)
		std::thread([time, execute]() {
		std::this_thread::sleep_for(std::chrono::milliseconds(time));
		execute();
	}).detach();
	else
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(time));
		execute();
	}
}

static bool DirectoryExists(string name)
{
	// use stat to determine if the given directory exists
	struct stat info;

	// check if an object exists with the given name
	if (stat(name.c_str(), &info) != 0)
		return false;

	// check if this is a directory
	if (info.st_mode & S_IFDIR)
		return true;

	// this isn't a directory so return false
	return false;
}

static bool FileExists(string name)
{
	// use stat to determine if the given file exists
	struct stat info;

	// check if an object exists with the given name
	if (stat(name.c_str(), &info) != 0)
		return false;

	// check if this is a directory
	if (info.st_mode & S_IFDIR)
		return false;

	// this must be an existing file so return true
	return true;
}

#pragma pack(1)
struct Stats {
	unsigned short health;
	unsigned short energy;
	unsigned short maxhealth;
	unsigned short maxenergy;

	static unsigned char* ToBuffer(Stats& stats) { return reinterpret_cast<unsigned char*>(&stats); }
	static Stats& FromBuffer(unsigned char* buffer) { return *reinterpret_cast<Stats*>(buffer); }
};
#pragma pack()

#ifdef _WIN32
static std::wstring ToWideString(string str)
{
	// convert the string to a wide string (mainly used for directory creation on Windows)
	const size_t len = str.length() + 1;
	std::wstring wstr(len, L'#');

	size_t outlen;
	mbstowcs_s(&outlen, &wstr[0], len, str.c_str(), len - 1);

	return wstr;
}
#endif

static string ToLower(string s)
{
	string out = "";
	std::locale loc;
	for (GLuint i = 0; i < s.length(); i++) {
		out += std::tolower(s[i], loc);
	}
	return out;
}

static std::streampos FileSize(std::fstream& file)
{
	// get the size of this file using tellg
	file.seekg(0, std::ios::end);
	std::streampos pos = file.tellg();
	file.seekg(0, std::ios::beg);

	// return the stream position
	return pos;
}

template<typename T> static void Limit(T &var, T lower, T upper)
{
	if (var < lower)
	{
		var = lower;
	}
	else if (var > upper)
	{
		var = upper;
	}
}

#endif
