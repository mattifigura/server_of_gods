#include "Server.h"
#include <iostream>
#include <fstream>
#include <chrono>

void TerrainThread(TerrainManager* t)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	std::cout << "Terrain management started..." << std::endl;
	
	t->Update();
}

bool Server::m_Running = false;

Server::Server()
	: m_PortNumber(8076)
	, m_Initialised(false)
	, m_MaxClients(32)
	, m_TimeOfDay(0.25f)
	, m_ServerName("Server Of Gods")
	, m_WorldName("World")
	, m_WorldSeed(1024)
	, m_BiomesFile("xml/biomes.xml")
{
	// set the seed to a random value
	std::chrono::duration<double> time = std::chrono::system_clock::now().time_since_epoch();
	m_WorldSeed = time.count();
	
	// read from the configuration file
	std::ifstream file("configofgods.cfg");
	if (!file.is_open())
	{
		// the server configuration file does not exist, write the default settings to the file
		WriteServerConfig();

		// re-open the file
		file.open("configofgods.cfg");
	}

	// now we should have the config file open for reading, let's sort out the server settings
	string line;
	while (std::getline(file, line, '\n'))
	{
		// split up the line
		size_t split = line.find('=');
		if (split != string::npos)
			ReadConfigSetting(line.substr(0, split), line.substr(split + 1));
	}

	// close the config file
	file.close();

	// function to print separator
	auto print_separator = [](string name) {
		for (GLuint i = 0; i < name.length() + 10; i++) {
			std::cout << "#";
		}
		std::cout << "##" << std::endl;
	};
	
	// sort out the server connection
	print_separator(m_ServerName);
	std::cout << "# Server: " << m_ServerName << " #" << std::endl;
	print_separator(m_ServerName);

#ifdef _WIN32
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
		std::cout << std::endl << "Error starting server: " << WSAGetLastError() << std::endl;
	else
	{
#endif
		std::cout << std::endl << "Initialised..." << std::endl;

		m_Socket = socket(AF_INET, SOCK_DGRAM, 0);
		if (m_Socket == INVALID_SOCKET)
#ifdef _WIN32
			std::cout << "Error starting server: " << WSAGetLastError() << std::endl;
#else
			std::cout << "Error starting server: " << errno << std::endl;
#endif
		else
		{
			m_Server.sin_family = AF_INET;
			m_Server.sin_addr.s_addr = INADDR_ANY;
			m_Server.sin_port = htons(m_PortNumber);

			// set the timeout of the socket to 3 seconds
			// to prevent us from getting stuck waiting on the recvfrom call
			const unsigned int timeout = 3; // seconds
#ifdef _WIN32
			DWORD timeout = timeout * 1000;
			setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);
#else
			struct timeval tv;
			tv.tv_sec = timeout;
			tv.tv_usec = 0;
			setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
#endif

			if (bind(m_Socket, (sockaddr*)&m_Server, sizeof(m_Server)) == SOCKET_ERROR)
			{
#ifdef _WIN32
				closesocket(m_Socket);
				std::cout << "Error starting server: " << WSAGetLastError() << std::endl;
#else
				close(m_Socket);
				std::cout << "Error starting server: " << errno << std::endl;
#endif
			}
			else
			{
				std::cout << "Socket initialised. Listening on port " << m_PortNumber << "..." << std::endl;
				std::cout << "Starting server... " << std::endl;
				m_Initialised = true;
				m_Running = true;

				// create clients
				m_Clients = new Client*[m_MaxClients];
				for (GLuint i = 0; i < m_MaxClients; i++) {
					m_Clients[i] = NULL;
				}

				// load in items
				Item::LoadItems("xml/weapons.xml", "weapon");
				Item::LoadItems("xml/armour.xml", "armour");
				Item::LoadItems("xml/tools.xml", "tool");
				Item::LoadItems("xml/ingots.xml", "ingot");
				Item::LoadItems("xml/blocks.xml", "block");
				Item::LoadItems("xml/ammo.xml", "ammo");
				Item::LoadItems("xml/projectiles.xml", "projectile");
				Item::SortItems();
				MobManager::LoadMobs("xml/enemies.xml", "enemy");

				// create counters
				for (GLuint c = 0; c < m_MaxClients; c++) {
					m_TxCounters.push_back(vector<GLuint>(max_message_number, 0));
					m_RxCounters.push_back(vector<GLuint>(max_message_number, 0));
				}

				// create terrain thread
				m_pTerrain = new TerrainManager(this, m_WorldName, m_WorldSeed, m_BiomesFile);

				if (!m_pTerrain->LoadBiomes())
				{
					m_Running = false;
					std::cout << "Cannot load biomes, is the XML file missing?" << std::endl;
				}

				m_pTerrainThread = new std::thread(TerrainThread, m_pTerrain);
				m_pTerrainThread->detach();

				// create client thread
				m_pClientThread = new std::thread(Server::ClientThread, this);
				m_pClientThread->detach();

				// create world thread
				m_pWorldThread = new std::thread(Server::WorldThread, this);
				m_pWorldThread->detach();

				// create server tick thread
				m_pServerTickThread = new std::thread(Server::ServerTickThread, this);
				m_pServerTickThread->detach();

				// create mob manager thread
				m_pMobMan = new MobManager(this, m_pTerrain);
				m_pMobManThread = new std::thread(MobManager::ThreadTask, m_pMobMan);
				m_pMobManThread->detach();
			}
		}
#ifdef _WIN32
	}
#endif
}

void Server::Run()
{
	// if we failed to initialise, quit now
	if (!m_Initialised)
		return;

	// create a temporary buffer
	char* buffer = new char[BUFFER_SIZE];
	int rlen;
#ifdef _WIN32
	int slen = sizeof(m_Other);
#else
	unsigned int slen = sizeof(m_Other);
#endif

	// define the quit function
	auto quit = [&]() {
		// cleanup the socket
#ifdef _WIN32
		closesocket(m_Socket);
		WSACleanup();
#else
		close(m_Socket);
#endif

		// save the terrain data
		m_pTerrain->SaveTerrain();
		m_pTerrain->Shutdown();

		// delete the client thread
		if (m_pClientThread)
			delete m_pClientThread;

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		// delete the world thread
		if (m_pWorldThread)
			delete m_pWorldThread;

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		// delete the server tick thread
		if (m_pServerTickThread)
			delete m_pServerTickThread;

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		// delete the mob manager thread
		if (m_pMobManThread)
			delete m_pMobManThread;

		std::this_thread::sleep_for(std::chrono::milliseconds(150));

		// delete the terrain thread
		if (m_pTerrainThread)
			delete m_pTerrainThread;

		std::this_thread::sleep_for(std::chrono::milliseconds(50));

		// delete the terrain manager
		if (m_pTerrain)
			delete m_pTerrain;

		// wait a bit for the threads to finish
		std::this_thread::sleep_for(std::chrono::milliseconds(700));

		// unload loaded items and mobs
		Item::UnloadItems();
		MobManager::UnloadMobs();

		// delete the clients
		for (GLuint i = 0; i < m_MaxClients; i++) {
			if (m_Clients[i])
			{
				if (m_Clients[i]->hotbar.len > 0)
					delete[] m_Clients[i]->hotbar.data;

				if (m_Clients[i]->equipment.len > 0)
					delete[] m_Clients[i]->equipment.data;

				for (GLuint j = 0; j < ACK_MSG_COUNT; j++) {
					delete[] m_Clients[i]->ackmsgs[j].buf;
				}
			}

			delete m_Clients[i];
			m_Clients[i] = NULL;
		}
		delete[] m_Clients;
		//m_Clients.clear();

		// delete items
		m_Items.clear();

		// lastly, write the server config
		WriteServerConfig();
	};

	// the main server loop - this receives data and passes it to the correct thread for processing
	// or in the case of a new client, creates a new thread to deal with the data
	while (m_Running)
	{
		// clear the buffer
		memset(buffer, 0, BUFFER_SIZE);

		// wait to receive some data
		rlen = recvfrom(m_Socket, buffer, BUFFER_SIZE, 0, (sockaddr*)&m_Other, &slen);
		/*if (rlen == SOCKET_ERROR)
		{
			_out << "Socket error: " << WSAGetLastError() << std::endl;
			m_Running = false;
			break;
		}*/

		// call processdata to process the data
		if (rlen > 0)
			ProcessData((unsigned char*)buffer, rlen);
	}

	// clean up
	delete[] buffer;

	// close the socket and clean up
	quit();
}

bool Server::IsRunning()
{
	return m_Running;
}

bool Server::GetListOfClients(std::vector<Client*>& out)
{
	// get a list of all the clients connected to the server
	// returns true if there are any connected clients, false if there are none
	for (int i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i])
			out.push_back(m_Clients[i]);
	}

	return !out.empty();
}

Client** Server::GetWholeListOfClients() const
{
	return m_Clients;
}

int Server::GetMaxClientCount()
{
	return m_MaxClients;
}

void Server::ProcessCommand(const string& in)
{
	// either someone has rcon'd into the server and issued a command, or they have run a command from the terminal/command line
	// function to check for a command name
	auto check_command_name = [&in](string command) -> bool {
		if (in.length() >= command.length())
		{
			for (GLuint i = 0; i < command.length(); i++) {
				if (in[i] != command[i])
					return false;
			}

			return true;
		}

		return false;
	};

	// function to split up input into sections
	auto split_input = [&in](vector<string>& out) {
		std::stringstream ss;
		
		for (GLuint i = 0; i < in.length(); i++) {
			if (in[i] != ' ' && in[i] != '\n' && in[i] != '\0')
			{
				// add to the current word
				ss << in[i];
			}
			else
			{
				// output a word
				out.push_back(ss.str());
				ss.str("");
			}
		}

		out.push_back(ss.str());
	};

	// function to print correct command usage
	auto correct_usage = [](const string& out) {
		_out << "Error! Correct usage is: " << out << _end;
	};

	// split up the input into arguments
	vector<string> args;
	split_input(args);

	// check for each command and see if we can execute it
	if (check_command_name("shutdown"))
	{
		// close the server
		_out << "Server shutting down..." << std::endl;

		// kick all players

		// close the socket
#ifdef _WIN32
		closesocket(m_Socket);
#else
		close(m_Socket);
#endif
		m_Running = false;
	}
	else if (check_command_name("timeofday"))
	{
		// set the time of day
		if (args.size() > 1)
			m_TimeOfDay = atof(args[1].c_str());
	}
	else if (check_command_name("say"))
	{
		// send a chat message
		int size = in.length() + HEADER_SIZE;
		unsigned char* buf = new unsigned char[size];
		buf[0] = 0;
		buf[1] = chat_message;
		buf[HEADER_SIZE] = 255; // 255 is server id
		SetBit(buf[0], 0, 1);

		// add the message
		for (GLuint i = 4; i < in.length(); i++) {
			buf[i + HEADER_SIZE - 3] = (unsigned char)in[i];
		}

		// send the message
		SendToAllClients((char*)buf, size);
	}
	else if (check_command_name("kick"))
	{
		if (args.size() < 2)
		{
			correct_usage("kick <player name>");
			return;
		}

		// check if the client exists
		Client* client = GetClientFromName(args[1]);
		if (client)
		{
			// send a kick packet to all clients
			int id = client->clientid;
			unsigned char buf[HEADER_SIZE + 2];
			memset(buf, 0, HEADER_SIZE);
			SetBit(buf[0], 0, 1);
			buf[1] = server_packet;
			buf[HEADER_SIZE] = sp_kick_player;
			buf[HEADER_SIZE + 1] = id;
			SendToAllClients((char*)buf, HEADER_SIZE + 2);

			// send a disconnect packet to all clients
			memset(buf, 0, HEADER_SIZE);
			SetBit(buf[0], 0, 1);
			buf[1] = disconnect;
			buf[HEADER_SIZE] = id;
			SendToAllOtherClients((char*)buf, HEADER_SIZE + 1, id);
			AddAckMsgToAllOtherClients(buf, HEADER_SIZE + 1, id);

			// delete the client
			if (client->hotbar.len > 0)
				delete[] client->hotbar.data;

			if (client->equipment.len > 0)
				delete[] client->equipment.data;

			for (GLuint j = 0; j < ACK_MSG_COUNT; j++) {
				delete[] client->ackmsgs[j].buf;
			}

			delete m_Clients[id];
			m_Clients[id] = NULL;
		}
		else
		{
			// client doesn't exist
			_out << "Error: Client with name " << args[1] << " is not connected to this server!" << _end;
		}
	}
	else if (check_command_name("giveitem"))
	{
		// give an item to the given player
		if (args.size() < 4)
		{
			correct_usage("giveitem <player name> <item> <amount>");
			return;
		}

		// check if the client exists
		Client* client = GetClientFromName(args[1]);
		if (client)
		{
			// get the name of the item
			string item;
			for (GLuint i = 2; i < args.size() - 1; i++) {
				item.append(args[i]);

				if (i < args.size() - 2)
					item.append(" ");
			}

			// get the amount of the item
			short amount = atoi(args[args.size() - 1].c_str());

			// get the itemclass
			const ItemClass* ic = Item::GetItemClass(item);
			if (!ic)
				return;

			// create the item
			ItemData data;
			data.index = ic->index;
			data.amount = amount;
			data.pos.Set(0, 0, 0);

			// add the client id on the front
			const size_t ilen = sizeof(ItemData);
			unsigned char* sbuf = new unsigned char[ilen + 2];
			sbuf[0] = sp_give_item;
			sbuf[1] = client->clientid;
			memcpy(&sbuf[2], ItemDataToBuffer(data), ilen);

			// send the packet and clean up
			SendPacket(client->clientid, server_packet, sbuf, ilen + 2);
			delete[] sbuf;
		}
		else
		{
			// client doesn't exist
			_out << "Error: Client with name " << args[1] << " is not connected to this server!" << _end;
		}
	}
	else if (check_command_name("resetchunk"))
	{
		// reset the chunk with the given coordinates
		if (args.size() < 4)
		{
			correct_usage("resetchunk <x> <y> <z>");
			return;
		}

		// get the chunk coordinates
		vector3di chunkPos(atoi(args[1].c_str()), atoi(args[2].c_str()), atoi(args[3].c_str()));

		// re-generate the chunk and send it to all clients
		m_pTerrain->GenerateTerrain(chunkPos, true);
		for (GLuint i = 0; i < m_MaxClients; i++) {
			Client* c = m_Clients[i];

			if (c)
			{
				// send this new chunk to this client
				m_pTerrain->SendChunk(chunkPos, m_Socket, c->sinfo, NextTxCounter(i, terrain_block_data));
			}
		}
	}
	else if (check_command_name("sethealth"))
	{
		if (args.size() < 3)
		{
			correct_usage("sethealth <player name> <amount>");
			return;
		}

		// check if the client exists
		Client* client = GetClientFromName(args[1]);
		if (client)
		{
			// check this client's max health to ensure we don't go over
			Stats& stats = client->stats;
			short value = atoi(args[2].c_str());
			if (value > stats.maxhealth || value < 0)
			{
				_out << "Error: Value out of range" << std::endl;
				return;
			}

			// this health is acceptable, change this client's health and send the new data
			unsigned short health = atoi(args[2].c_str());
			SetClientHealth(client->clientid, health);
		}
	}
	else if (check_command_name("move"))
	{
		if (args.size() < 5)
		{
			correct_usage("move <player name> x y z");
			return;
		}

		// check if the client exists
		Client* client = GetClientFromName(args[1]);
		if (client)
		{
			// get the x, y and z positions to move to
			vector3df pos(atof(args[2].c_str()) * 10.f, atof(args[3].c_str()) * 10.f, atof(args[4].c_str()) * 10.f);

			// move the client
			MoveClient(client->clientid, pos);
		}
	}
	else
	{
		_out << "Error: unknown command \"" << in << "\"." << std::endl;
	}
}

/*void TestThread(TerrainManager* t, SOCKET socket, sockaddr_in& other)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	t->SendChunk(vector3di(), socket, other);
}*/

void Server::WriteServerConfig()
{
	// write the server's configuration to the config file, this gets called if the server admin changes any settings during run time
	// or if the server configuration file doesn't exist, in which case this will create it
	std::ofstream file("configofgods.cfg", std::ofstream::out);

	// write the file header
	file << "###################################\n#Server Of Gods Configuration File#\n###################################\n\n";

	// write the general server settings
	file << "#General Settings\n";
	file << "port=" << m_PortNumber;
	file << "\nname=" << m_ServerName;
	file << "\nmaxplayers=" << m_MaxClients;

	// write the world settings
	file << "\n\n#World\n";
	file << "worldname=" << m_WorldName;
	file << "\nseed=" << m_WorldSeed;
	file << "\nbiomesfile=" << m_BiomesFile;

	// finally, write a new line and close the file
	file << "\n";
	file.close();
}

void Server::ReadConfigSetting(string name, string value)
{
	// if the value/name string is empty then do nothing
	if (name.empty() || value.empty())
		return;

	// if the name string begins with a #, ignore it
	if (name[0] == '#')
		return;
	
	// set the server setting based on the name
	if (name == "port")
	{
		m_PortNumber = atoi(value.c_str());
	}
	else if (name == "name")
	{
		m_ServerName = value;
	}
	else if (name == "maxplayers")
	{
		m_MaxClients = atoi(value.c_str());

		if (m_MaxClients > 255)
			m_MaxClients = 255;
		else if (m_MaxClients < 1)
			m_MaxClients = 1;
	}
	else if (name == "worldname")
	{
		m_WorldName = value;
	}
	else if (name == "seed")
	{
		m_WorldSeed = atol(value.c_str());
	}
	else if (name == "biomesfile")
	{
		m_BiomesFile = value;
	}
}

int Server::GetFreeClientID()
{
	// loop through the clients to find a free ID
	for (int i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i] == NULL)
			return i;
	}

	// we can't find a free client ID, server is full
	return -1;
}

bool Server::SendPacket(int clientid, unsigned char type, unsigned char* buf, int size, bool ack, bool req)
{
	// if the client doesn't exist, don't do anything
	Client* c = GetClientFromID(clientid);
	if (!c)
		return false;
	
	// send a packet to the given client
	char* sendbuf;
	if (buf)
	{
		// if the user has supplied data, copy the data to a char buffer
		sendbuf = new char[size + HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;

		if (req)
			SetBit(sendbuf[0], 2, 1);

		memcpy(&sendbuf[HEADER_SIZE], buf, size);
		size += HEADER_SIZE;
	}
	else
	{
		// if the user has supplied no data (ie for a request packet), send 2 bytes
		sendbuf = new char[HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;
		SetBit(sendbuf[0], 2, 1);
		size = HEADER_SIZE;
	}

	// set the server bit
	SetBit(sendbuf[0], 0, 1);

	// add the counter
	UIntToByte(NextTxCounter(clientid, type), (unsigned char*)&sendbuf[2]);

	// send the packet
	int ret = sendto(m_Socket, sendbuf, size, 0, (sockaddr*)&c->sinfo, sizeof(c->sinfo));

	// add this to this client's ack messages
	if (ack)
		c->AddAckMsg(MessageHeader::FromBuffer((unsigned char*)sendbuf), (unsigned char*)sendbuf, size);

	// clean up
	delete[] sendbuf;

	// return whether we were successful
	return (ret > 0);
}

bool Server::SendPacketToAllClients(unsigned char type, unsigned char* buf, int size, bool ack, bool req)
{
	// send a packet to the given client
	char* sendbuf;
	if (buf)
	{
		// if the user has supplied data, copy the data to a char buffer
		sendbuf = new char[size + HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;

		if (req)
			SetBit(sendbuf[0], 2, 1);

		memcpy(&sendbuf[HEADER_SIZE], buf, size);
		size += HEADER_SIZE;
	}
	else
	{
		// if the user has supplied no data (ie for a request packet), send 2 bytes
		sendbuf = new char[HEADER_SIZE];
		sendbuf[0] = 0;
		sendbuf[1] = type;
		SetBit(sendbuf[0], 2, 1);
		size = HEADER_SIZE;
	}

	// set the server bit
	SetBit(sendbuf[0], 0, 1);

	for (GLuint i = 0; i < m_MaxClients; i++) {
		// check this client
		Client* c = m_Clients[i];
		if (!c)
			continue;

		// add the counter
		UIntToByte(NextTxCounter(c->clientid, type), (unsigned char*)&sendbuf[2]);

		// send the packet
		int ret = sendto(m_Socket, sendbuf, size, 0, (sockaddr*)&c->sinfo, sizeof(c->sinfo));

		// add this to this client's ack messages
		if (ack)
			c->AddAckMsg(MessageHeader::FromBuffer((unsigned char*)sendbuf), (unsigned char*)sendbuf, size);

		// if we failed to send, abort now
		if (ret <= 0)
		{
			delete[] sendbuf;
			return false;
		}
	}

	// clean up
	delete[] sendbuf;

	// we have been successful
	return true;
}

bool Server::SendAck(int clientid, unsigned char* buf, int size)
{
	// ensure the client exists
	Client* client = m_Clients[clientid];
	if (!client)
		return false;
	
	// send an ack response back to the client
	SetBit(buf[0], 0, 1);
	SetBit(buf[0], 1, 1);

	// send the packet
	int ret = sendto(m_Socket, (char*)buf, HEADER_SIZE + size, 0, (sockaddr*)&client->sinfo, sizeof(client->sinfo));

	if (ret == SOCKET_ERROR)
		// if there was a socket error alert us (for DEBUG only)
#ifdef _WIN32
		std::cout << "SOCKET ERROR" << WSAGetLastError() << std::endl;
#else
		std::cout << "SOCKET ERROR" << errno << std::endl;
#endif

	return (ret >= 0);
}

ItemData& Server::CreateItem(string name, vector3df pos, short amount)
{
	// find the itemclass with this name
	const ItemClass* ic = Item::GetItemClass(name);
	
	// create a network item, add it to the list and send it to all clients
	ItemData item;

	// create the item
	item.index = ic->index;
	item.pos = pos;
	item.amount = amount;

	// add the item to our list
	m_Items.push_back(item);

	// create the message
	const size_t ilen = sizeof(ItemData);
	unsigned char* sbuf = new unsigned char[ilen + HEADER_SIZE + 1];
	sbuf[0] = 0;
	sbuf[1] = create_item;
	sbuf[HEADER_SIZE] = 255; // 255 is max value for unsigned char, also WOULD be 256th player but there is a 255 player limit
	SetBit(sbuf[0], 0, 1);
	memcpy(&sbuf[HEADER_SIZE + 1], ItemDataToBuffer(item), ilen);

	// send the packet and clean up
	SendToAllClients((char*)sbuf, ilen + HEADER_SIZE + 1);
	delete[] sbuf;

	// return a reference to the new item
	return (*--m_Items.end());
}

GLuint Server::NextTxCounter(int client, int message)
{
	return m_TxCounters.at(client).at(message)++;
}

GLuint Server::GetTxCounter(int client, int message) const
{
	return m_TxCounters.at(client).at(message);
}

bool Server::SetTxCounter(int client, int message, GLuint counter)
{
	if (counter > GetTxCounter(client, message))
	{
		m_TxCounters.at(client).at(message) = counter;
		return true;
	}

	return false;
}

void Server::ResetTxCounter(int client, int message)
{
	m_TxCounters.at(client).at(message) = 0;
}

GLuint Server::GetRxCounter(int client, int message) const
{
	return m_RxCounters.at(client).at(message);
}

bool Server::SetRxCounter(int client, int message, GLuint counter)
{
	if (counter > GetRxCounter(client, message))
	{
		m_RxCounters.at(client).at(message) = counter;
		return true;
	}

	return false;
}

void Server::ResetRxCounter(int client, int message)
{
	m_RxCounters.at(client).at(message) = 0;
}

void Server::ResetAllCounters(int client)
{
	m_RxCounters.at(client).clear();
	m_TxCounters.at(client).clear();

	m_RxCounters.at(client).resize(max_message_number, 0);
	m_TxCounters.at(client).resize(max_message_number, 0);
}

void Server::SendToAllClients(char* buf, int size)
{
	// send the message to all clients connected to the server
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i])
		{
			UIntToByte(NextTxCounter(i, buf[1]), (unsigned char*)&buf[2]);
			sendto(m_Socket, buf, size, 0, (sockaddr*)&m_Clients[i]->sinfo, sizeof(m_Clients[i]->sinfo));
		}
	}
}

void Server::SendToAllOtherClients(char* buf, int size, int exclude)
{
	// send the message to all clients connected to the server other than exclude
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (!m_Clients[i])
			continue;

		if (m_Clients[i]->clientid == exclude)
			continue;

		UIntToByte(NextTxCounter(i, buf[1]), (unsigned char*)&buf[2]);
		sendto(m_Socket, buf, size, 0, (sockaddr*)&m_Clients[i]->sinfo, sizeof(m_Clients[i]->sinfo));
	}
}

void Server::AddAckMsgToAllClients(unsigned char* buf, int size)
{
	// add this message as an ack message to all clients
	MessageHeader header = MessageHeader::FromBuffer(buf);
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i])
		{
			header.counter = GetTxCounter(i, header.message) - 1;
			m_Clients[i]->AddAckMsg(header, buf, size);
		}
	}
}

void Server::AddAckMsgToAllOtherClients(unsigned char* buf, int size, int exclude)
{
	// add this message as an ack message to all clients other than exclude
	MessageHeader header = MessageHeader::FromBuffer(buf);
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i] && m_Clients[i]->clientid != exclude)
		{
			header.counter = GetTxCounter(i, header.message) - 1;
			m_Clients[i]->AddAckMsg(header, buf, size);
		}
	}
}

void Server::ProcessData(unsigned char* buf, int rlen)
{
	// get info common to all messages
	bool server = GetBit(buf[0], 0);

	if (server)
		// this message should not have been received at the server, discard it
		return;

	// local delta time used for this function
	static std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
	std::chrono::time_point<std::chrono::system_clock> newtime = std::chrono::system_clock::now();
	std::chrono::duration<double> duration = newtime - time;

	double delta = duration.count();
	time = newtime;

	// extract the message header
	MessageHeader& header = MessageHeader::FromBuffer(buf);

	bool ack = GetBit(header.info, 1);
	bool req = GetBit(header.info, 2);

	// reset the timeout for this client
	Client* c = GetClientFromSockAddrInfo(m_Other);
	if (c)
		c->timeout = 0;

	// switch based on message type
	switch (header.message)
	{
		case server_packet:
		{
			switch (buf[HEADER_SIZE])
			{
				case sp_move_player:
				{
					// a client has acknowledged us telling them to move
					if (ack)
						AcknowledgeMessage(header, m_Other);

					break;
				}
				case sp_knockback:
				{
					// a client has acknowledged us telling them to knockback
					if (ack)
						AcknowledgeMessage(header, m_Other);

					break;
				}
			}
		}
		case new_connection:
		{
			// acknowledge
			if (ack)
			{
				// a client has acknowledged a disconnect
				AcknowledgeMessage(header, m_Other);
				break;
			}
			
			// check first if this client exists, it might have crashed/lost connection and be wanting to reconnect
			Client* ec = GetClientFromSockAddrInfo(m_Other);
			if (ec != NULL)
			{
				ec->newlyconnected = true;
				break;
			}
			
			// a client is wanting to connect, look for a free client slot
			int id = GetFreeClientID();

			// if the server is full then don't do anything
			if (id < 0)
				break;

			// reply with a connection accepted message and their ID
			SetBit(buf[0], 0, 1);
			SetBit(buf[0], 1, 1);
			buf[HEADER_SIZE] = id;
			UIntToByte(NextTxCounter(id, new_connection), &buf[2]);
			sendto(m_Socket, (char*)buf, rlen, 0, (sockaddr*)&m_Other, sizeof(m_Other));
			SetBit(buf[0], 1, 0);

			// create a new client object and run that in a new thread
			//m_pTerrain->SendChunk(vector3di(), m_Socket, &m_Other);
			Client* c = new Client;
			
			// newly connected flag
			c->newlyconnected = true;

			// client id
			c->clientid = id;

			// connection info
			memcpy(&c->sinfo, &m_Other, sizeof(c->sinfo));
			c->timeout = 0;

			// inventory info
			memset(&c->hotbar, 0, sizeof(c->hotbar));
			memset(&c->equipment, 0, sizeof(c->equipment));
			for (GLuint i = 0; i < 14; i++) {
				InventoryItem item;
				item.ic = NULL;
				item.name = "";
				c->equipment.items.push_back(item);

				if (i < 10)
					c->hotbar.items.push_back(item);
			}
			c->attack = 0.f;

			// stats
			memset(&c->stats, 0, sizeof(c->stats));
			c->statsreceived = false;
			c->deathtimerstarted = false;
			c->hitcount = 0;
			c->hpregen = 0;
			c->hpregenspeed = 0.05f;
			c->mpregen = 0;
			c->mpregenspeed = 0.1f;
			c->biome = -1;

			// read in the spawn location for this client's file
			if (true/*ReadClientSpawnPoint(...)*/)
			{
				// the spawn location couldn't be read in, this is probably a new character, set its spawn point to default
				c->spawn.Set(0, m_pTerrain->GetHeightBelow(vector3df(0, 200, 0)), 0);
			}

			// create the ack messages
			for (GLuint i = 0; i < ACK_MSG_COUNT; i++) {
				AckMsg msg;
				msg.isfree = true;
				msg.buf = new unsigned char[BUFFER_SIZE];
				msg.len = 0;

				c->ackmsgs[i] = msg;
			}

			// client name
			c->name = "";
			int i = HEADER_SIZE + 1;
			while (buf[i] != '\0')
			{
				// add to the text
				c->name += (char)buf[i++];

				if (i == rlen)
					// this is here as a fail-safe incase something went wrong with the transmission
					break;
			}

			//std::cout << c->name << std::endl;

			// add the new client
			//m_Clients.push_back(c);
			m_Clients[id] = c;

			//c->pthread = new std::thread(Server::ClientThread, this, c);
			//c->pthread->detach();

			// send this new connection to all active clients
			SendToAllOtherClients((char*)buf, rlen, id);
			AddAckMsgToAllOtherClients(buf, rlen, id);

			// now send all other connected clients to this new client
			unsigned char* cbuf = new unsigned char[BUFFER_SIZE];
			cbuf[0] = 0;
			cbuf[1] = new_connection;
			SetBit(cbuf[0], 0, 1);
			MessageHeader newheader = MessageHeader::FromBuffer(cbuf);
			for (GLuint y = 0; y < m_MaxClients; y++) {
				if (m_Clients[y] && m_Clients[y]->clientid != id)
				{
					cbuf[HEADER_SIZE] = m_Clients[y]->clientid;
					memset(&cbuf[HEADER_SIZE + 1], 0, BUFFER_SIZE - HEADER_SIZE - 1);
					for (GLuint j = 0; j < m_Clients[y]->name.length(); j++) {
						cbuf[j + HEADER_SIZE + 1] = (unsigned char)m_Clients[y]->name[j];
					}
					UIntToByte(NextTxCounter(id, new_connection), &cbuf[2]);
					sendto(m_Socket, (char*)cbuf, m_Clients[y]->name.length() + HEADER_SIZE + 1, 0, (sockaddr*)&c->sinfo, sizeof(c->sinfo));
					newheader.counter = GetTxCounter(id, newheader.message) - 1;
					c->AddAckMsg(newheader, cbuf, m_Clients[y]->name.length() + HEADER_SIZE + 1);
				}
			}
			delete[] cbuf;

			// now move the client to their spawn point
			MoveClient(c->clientid, c->spawn);
			//Timer(1000, true, ReviveClient, this, c);

			// alert us that a new player has connected
			_out << "New client connected from " << inet_ntoa(m_Other.sin_addr) << ": " << c->name << " ID [" << id << "]" << _end;
			break;
		}
		case disconnect:
		{
			if (ack)
			{
				// a client has acknowledged a disconnect
				AcknowledgeMessage(header, m_Other);
			}
			else
			{
				// a client is disconnecting, delete them
				int id = buf[HEADER_SIZE];
				string name("");

				if (m_Clients[id])
				{
					name = m_Clients[id]->name;
					
					if (m_Clients[id]->hotbar.len > 0)
						delete[] m_Clients[id]->hotbar.data;

					if (m_Clients[id]->equipment.len > 0)
						delete[] m_Clients[id]->equipment.data;

					for (GLuint j = 0; j < ACK_MSG_COUNT; j++) {
						delete[] m_Clients[id]->ackmsgs[j].buf;
					}
					
					delete m_Clients[id];
					m_Clients[id] = NULL;
				}

				// send the disconnect message to all other clients
				SetBit(buf[0], 0, 1);
				SendToAllOtherClients((char*)buf, rlen, id);
				AddAckMsgToAllOtherClients(buf, rlen, id);

				// alert us
				_out << "[" << id << "] " << name << " has disconnected." << _end;
			}
			break;
		}
		case player_data:
		{
			// update the player's position from this data
			int id = buf[HEADER_SIZE];
			Client* c = GetClientFromID(id);

			if (c != NULL)
			{
				// get the data
				PlayerData& data = PlayerDataFromBuffer(&buf[HEADER_SIZE + 1]);

				// update the client information
				c->data.pos.Set(data.pos);
				c->data.rot = data.rot;
				c->data.selected = data.selected;
				c->data.swing = data.swing;
				c->data.sprinting = data.sprinting;

				if (c->data.swing == 0)
					c->attack = 0.85f;

				//if (data.swing && c->stats.health > 0)
				//	ClientUsingItem(c, delta);
			}
			else
				_out << "Error finding client in list, does it still exist?" << _end;
			break;
		}
		case player_hotbar_data:
		{
			// a client has sent us their hotbar data, save the data and send it to all other clients
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}

			// firstly acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);

			int id = buf[HEADER_SIZE];
			Client* c = GetClientFromID(id);

			if (c != NULL)
			{
				if (req)
				{
					// the client exists and another client has request its hotbar data, send it
					sendto(m_Socket, (char*)c->hotbar.data, c->hotbar.len, 0, (sockaddr*)&m_Other, sizeof(m_Other));
				}
				else
				{
					// clean up
					if (c->hotbar.len > 0)
						delete[] c->hotbar.data;
					
					// the client exists, add their hotbar data
					SetBit(buf[0], 0, 1);
					unsigned char* data = new unsigned char[rlen];
					memcpy(data, buf, rlen);
					c->hotbar.data = data;
					c->hotbar.len = rlen;
					c->hotbar.newdata = true;

					// now send this to all other connected clients
					SendToAllOtherClients((char*)buf, rlen, id);
					AddAckMsgToAllOtherClients(buf, rlen, id);
				}
			}
			break;
		}
		case player_equipment_data:
		{
			// a client has sent us their equipment data, save the data and send it to all other clients
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}

			// firstly acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);

			int id = buf[HEADER_SIZE];
			Client* c = GetClientFromID(id);

			if (c != NULL)
			{
				if (req)
				{
					// the client exists and another client has request its equipment data, send it
					sendto(m_Socket, (char*)c->equipment.data, c->equipment.len, 0, (sockaddr*)&m_Other, sizeof(m_Other));
				}
				else
				{
					// clean up
					if (c->equipment.len > 0)
						delete[] c->equipment.data;

					// the client exists, add their equipment data
					SetBit(buf[0], 0, 1);
					unsigned char* data = new unsigned char[rlen];
					memcpy(data, buf, rlen);
					c->equipment.data = data;
					c->equipment.len = rlen;
					c->equipment.newdata = true;

					// now send this to all other connected clients
					SendToAllOtherClients((char*)buf, rlen, id);
					AddAckMsgToAllOtherClients(buf, rlen, id);
				}
			}
			break;
		}
		case player_stat_data:
		{
			// a client has sent us their initial stat data
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}

			// firstly acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);

			int id = buf[HEADER_SIZE];
			Client* c = GetClientFromID(id);

			if (c && !c->statsreceived)
			{
				// we have not yet received stats about this player
				c->stats = Stats::FromBuffer(&buf[HEADER_SIZE + 1]);
				c->statsreceived = true;
			}
			break;
		}
		case player_action:
		{
			// a client has told us they have performed an action
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
			}

			// firstly acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);

			// get the client
			int id = buf[HEADER_SIZE];
			Client* c = GetClientFromID(id);

			if (c)
			{
				// let us move into a separate function to process this
				ProcessClientAction(c, buf[HEADER_SIZE + 1]);
			}

			break;
		}
		case terrain_block_data:
		{
			if (!ack && req)
			{
				// a client is requesting terrain block data for a chunk they have no information on

				// ensure the client exists
				Client* client = GetClientFromSockAddrInfo(m_Other);
				if (!client)
					break;

				// extract the chunk coordinates
				int x, y, z;
				ByteToInt(&buf[HEADER_SIZE], x);
				ByteToInt(&buf[HEADER_SIZE + 4], y);
				ByteToInt(&buf[HEADER_SIZE + 8], z);
				vector3di pos(x, y, z);

				// send the data
				m_pTerrain->SendChunk(pos, m_Socket, m_Other, NextTxCounter(client->clientid, terrain_block_data));

				// for debug
				//_out << "Terrain data requested for chunk at " << x << ", " << y << ", " << z << _end;
			}

			if (ack)
				AcknowledgeMessage(header, m_Other);
			break;
		}
		case terrain_water_data:
		{
			if (!ack && req)
			{
				// a client is requesting terrain water data for a chunk they have no information on

				// ensure the client exists
				Client* client = GetClientFromSockAddrInfo(m_Other);
				if (!client)
					break;

				// extract the chunk coordinates
				int x, y, z;
				ByteToInt(&buf[HEADER_SIZE], x);
				ByteToInt(&buf[HEADER_SIZE + 4], y);
				ByteToInt(&buf[HEADER_SIZE + 8], z);
				vector3di pos(x, y, z);

				// send the data
				m_pTerrain->SendWater(pos, m_Socket, m_Other, NextTxCounter(client->clientid, terrain_water_data));

				// for debug
				//std::cout << "Terrain data requested for chunk at " << x << ", " << y << ", " << z << std::endl;
			}

			if (ack)
				AcknowledgeMessage(header, m_Other);
			break;
		}
		case change_block:
		{
			if (!ack)
			{
				// update the terrain on the server - this might update the buffer and rlen (ie if we need to add block info on multi-block lists)
				m_pTerrain->ProcessChangeBlock(&buf[HEADER_SIZE + 1], rlen);

				// the check has passed, send the data
				SetBit(buf[0], 0, 1);
				SendToAllClients((char*)buf, rlen);
				AddAckMsgToAllClients(buf, rlen);

				// acknowledge the message
				SendAck(buf[HEADER_SIZE], buf);
			}
			else
			{
				// a client has acknowledged the block change message
				AcknowledgeMessage(header, m_Other);
			}
			break;
		}
		case create_item:
		{
			// time to create a new item!
			ItemData item = ItemDataFromBuffer(&buf[HEADER_SIZE + 1]);
			m_Items.push_back(item);

			// forward this data onto all the other clients
			int id = buf[HEADER_SIZE];
			SetBit(buf[0], 0, 1);
			SendToAllOtherClients((char*)buf, rlen, id);

			// send an ack back to the client that sent us this message
			SetBit(buf[0], 1, 1);
			sendto(m_Socket, (char*)buf, rlen, 0, (sockaddr*)&m_Other, sizeof(m_Other));
			break;
		}
		case destroy_item:
		{
			// check if this item exists
			GLuint item;
			if (FindItem(ItemDataFromBuffer(&buf[HEADER_SIZE + 1]), item))
			{
				// this item still exists on the server, delete it
				// 2020 update: why didn't I just do m_Items.erase(m_Items.begin() + item); instead of this rubbish?
				vector<ItemData>::iterator it = m_Items.begin();
				GLuint i = 0;
				while (i < item)
				{
					it++; // 2020 update: should be ++it to prevent creating copies each iteration - does the compiler do this automatically these days though?
					i++;
				}
				m_Items.erase(it);

				// forward this data onto all other clients
				int id = buf[HEADER_SIZE];
				SetBit(buf[0], 0, 1);
				SendToAllOtherClients((char*)buf, rlen, id);

				// send an ack back to the client that sent us this message
				SetBit(buf[0], 1, 1);
				sendto(m_Socket, (char*)buf, rlen, 0, (sockaddr*)&m_Other, sizeof(m_Other));
			}
			else
			{
				// this item has been destroyed by another client already, send a destroy item message
				SetBit(buf[0], 0, 1);
				sendto(m_Socket, (char*)buf, rlen, 0, (sockaddr*)&m_Other, sizeof(m_Other));
			}
			break;
		}
		case chat_message:
		{
			// a client has typed something in chat, extract the text
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}

			// check the client exists
			int id = buf[HEADER_SIZE];
			if (GetClientFromID(id) == NULL)
				break;

			string text("");
			int i = HEADER_SIZE + 1;
			while (buf[i] != '\0')
			{
				// add to the text
				text += (char)buf[i++];

				if (i == rlen)
					// this is here as a fail-safe incase something went wrong with the transmission
					break;
			}

			// check the message to see if it's a command
			int to_be_added;

			// if this is not a command, send the chat message to all clients
			SetBit(buf[0], 0, 1);
			SendToAllClients((char*)buf, rlen);
			AddAckMsgToAllClients(buf, rlen);

			// acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);

			// print the message
			_out << "[" << id << "] " << GetClientFromID(id)->name << ": " << text << _end;
			break;
		}
		case block_interaction:
		{
			// acknowledge
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}
			
			// send this to the terrain manager to handle
			unsigned char tmp[BUFFER_SIZE];
			memcpy(tmp, buf, rlen);
			if (m_pTerrain->ProcessInteraction(&buf[HEADER_SIZE]))
			{
				// we can send this message to all clients
				SetBit(buf[0], 0, 1);
				SendToAllClients((char*)buf, 23);
				AddAckMsgToAllClients(buf, rlen);
			}

			// acknowledge the message
			SendAck(tmp[HEADER_SIZE], tmp);
			break;
		}
		case block_inventory:
		{
			// acknowledge
			if (ack)
			{
				AcknowledgeMessage(header, m_Other);
				break;
			}
			
			// send this to the terrain manager to handle
			m_pTerrain->ProcessBlockInventoryUpdate(&buf[HEADER_SIZE], rlen - HEADER_SIZE);

			// acknowledge the message
			SendAck(buf[HEADER_SIZE], buf);
			break;
		}
		/*case new_projectile:
		{
			if (ack)
				AcknowledgeMessage(header, m_Other);

			break;
		}*/
	}

	// temp: print acknowledgement
	//if (ack)
	//	std::cout << "Message has been acknowledged!" << std::endl;
}

void Server::ReviveClient(Server* s, Client* c)
{
	// ensure this player is still on the server
	if (!c)
		return;

	// ensure we aren't mistaken
	if (c->stats.health > 0)
	{
		c->deathtimerstarted = false;
		return;
	}
	
	// set this client's health to max again
	c->stats.health = c->stats.maxhealth;
	c->stats.energy = c->stats.maxenergy;
	c->deathtimerstarted = false;

	const size_t len = sizeof(Stats) + 1;
	unsigned char buf[len];
	buf[0] = c->clientid;
	memcpy(&buf[1], Stats::ToBuffer(c->stats), len - 1);

	s->SendPacketToAllClients(player_stat_data, buf, len, true);

	// tell this client to reset their position to their spawn position
	s->MoveClient(c->clientid, c->spawn);
}

void Server::ClientThread(Server* s)
{
	// keep this client up to date with information!
	while (m_Running)
	{
		for (GLuint i = 0; i < s->m_MaxClients && m_Running; i++) {
			Client* c = s->m_Clients[i];

			if (!c)
				continue;

			// check to see if this client has timed out
			if (++c->timeout > 150)
			{
				// the last message received from this client was too long ago, this client has timed out, tell everyone
				int id = c->clientid;
				string name("");

				name = c->name;

				if (c->hotbar.len > 0)
					delete[] c->hotbar.data;

				if (c->equipment.len > 0)
					delete[] c->equipment.data;

				for (GLuint j = 0; j < ACK_MSG_COUNT; j++) {
					delete[] c->ackmsgs[j].buf;
				}

				delete c;
				c = NULL;
				s->m_Clients[id] = NULL;

				// send the disconnect message to all other clients
				unsigned char buf[HEADER_SIZE + 1];
				SetBit(buf[0], 0, 1);
				buf[1] = disconnect;
				UIntToByte(s->NextTxCounter(id, disconnect), (unsigned char*)&buf[2]);
				buf[HEADER_SIZE] = id;
				s->SendToAllOtherClients((char*)buf, HEADER_SIZE + 1, id);
				s->AddAckMsgToAllOtherClients(buf, HEADER_SIZE + 1, id);

				// alert us
				_out << "[" << id << "] " << name << " has timed out." << _end;
				continue;
			}

			// hotbar data
			if (c->hotbar.newdata)
			{
				// unpack the items
				uLongf size = (10) * 300 + 300;
				unsigned char* data = new unsigned char[size];
				uncompress(data, &size, &c->hotbar.data[HEADER_SIZE + 1], c->hotbar.len - HEADER_SIZE - 1);

				// extract item names and whatnot
				GLuint pos = 0;
				const size_t ilen = sizeof(ItemData);
				for (GLuint i = 0; i < 10; i++) {
					// reset the item at this position
					c->hotbar.items.at(i).ic = NULL;
					c->hotbar.items.at(i).name = "";
					c->hotbar.items.at(i).amount = 0;

					// see if there's an item here
					bool itemHere = data[pos] > 0;
					pos++;

					if (itemHere)
					{
						// there is an item here, get it
						ItemData item = ItemDataFromBuffer(&data[pos]);

						// set the name and find the item class
						c->hotbar.items.at(i).ic = Item::GetItemClass(item.index);
						c->hotbar.items.at(i).name = c->hotbar.items.at(i).ic->strName;
						c->hotbar.items.at(i).amount = item.amount;

						// move on
						pos += ilen;
					}
				}

				// reset the flag and clean up
				delete[] data;
				c->hotbar.newdata = false;
			}

			// equipment data
			if (c->equipment.newdata)
			{
				// unpack the items
				uLongf size = (14) * 300 + 300;
				unsigned char* data = new unsigned char[size];
				uncompress(data, &size, &c->equipment.data[HEADER_SIZE + 1], c->equipment.len - HEADER_SIZE - 1);

				// extract item names and whatnot
				GLuint pos = 0;
				const size_t ilen = sizeof(ItemData);
				for (GLuint i = 0; i < 14; i++) {
					// reset the item at this position
					c->equipment.items.at(i).ic = NULL;
					c->equipment.items.at(i).name = "";
					c->equipment.items.at(i).amount = 0;

					// see if there's an item here
					bool itemHere = data[pos] > 0;
					pos++;

					if (itemHere)
					{
						// there is an item here, get it
						ItemData item = ItemDataFromBuffer(&data[pos]);

						// set the name and find the item class
						c->equipment.items.at(i).ic = Item::GetItemClass(item.index);
						c->equipment.items.at(i).name = c->equipment.items.at(i).ic->strName;
						c->equipment.items.at(i).amount = item.amount;

						// move on
						pos += ilen;
					}
				}

				// reset the flag and clean up
				delete[] data;
				c->equipment.newdata = false;

				// calculate this client's defences
				c->defence = 0;
				for (GLuint i = 0; i < 5; i++) {
					const InventoryItem& item = c->equipment.items.at(i + 9);

					if (item.ic)
						c->defence += item.ic->sValues[1];
				}
			}

			// client using an item
			if (c->data.swing > 0 && c->stats.health > 0)
				s->ClientUsingItem(c, 0.05);

			// respawn timer
			if (c->stats.health == 0 && !c->deathtimerstarted)
			{
				c->deathtimerstarted = true;
				Timer(10000, true, Server::ReviveClient, s, c);
			}

			// decrease the hit count for invulnerability purposes
			if (c->hitcount > 0)
				c->hitcount--;

			// health / mana regen
			if (c->stats.health > 0 && c->stats.health < c->stats.maxhealth)
			{
				c->hpregen += c->hpregenspeed;
				if (c->hpregen >= 1.f)
				{
					++c->stats.health;
					c->hpregen = 0;
					s->SetClientHealth(i, c->stats.health);
				}
			}

			if (c->stats.health > 0 && (c->stats.energy < c->stats.maxenergy || c->data.sprinting))
			{
				c->mpregen += c->mpregenspeed + (c->data.sprinting ? -0.025f : 0.f);
				if (c->mpregen >= 1.f)
				{
					if (c->data.sprinting && c->stats.energy > 0)
						--c->stats.energy;
					else
						++c->stats.energy;
					c->mpregen = 0;
					s->SetClientEnergy(i, c->stats.energy);
				}
			}

			if (c->newlyconnected)
			{
				// this client has just connected to the server so send them information about the chunks around them!
				vector3di pos;
				s->m_pTerrain->GetCornerChunkPosition(c->data.pos, pos);

				for (int i = pos.x; i < pos.x + 5; i++) {
					for (int j = pos.z; j < pos.z + 5; j++) {
						for (int k = pos.y; k < pos.y + 3; k++) {
							s->m_pTerrain->SendChunk(vector3di(i, k, j), s->m_Socket, c->sinfo, s->NextTxCounter(c->clientid, terrain_block_data));

							std::this_thread::sleep_for(std::chrono::milliseconds(5));
						}
					}
				}

				// remove the flag
				c->newlyconnected = false;
			}

			// deal with this client moving between biome borders
			Biome biome = s->m_pTerrain->GetBiome(c->data.pos);
			if (c->biome != biome.index)
			{
				// the player has crossed into a new biome, send them a new biome message to let them know
				c->biome = biome.index;

				// create the message
				const size_t len = biome.name.length();
				unsigned char* buf = new unsigned char[len + 3];

				// client id and server packet type
				buf[0] = sp_new_biome;
				buf[1] = c->clientid;

				// put the name string in
				for (GLuint i = 0; i < len; ++i) {
					buf[i + 2] = biome.name[i];
				}
				buf[len + 2] = '\0';

				// send the packet and clean up
				s->SendPacket(c->clientid, server_packet, buf, len + 3, true);
				delete buf;
			}

			// loop through the other clients and send their position data to this client
			for (GLuint j = 0; j < s->m_MaxClients; j++) {
				if (!s->m_Clients[j])
					continue;

				if (s->m_Clients[j]->clientid != c->clientid)
				{
					// create the message
					static size_t plen = sizeof(PlayerData);
					unsigned char* buf = new unsigned char[plen + 1];

					// get this client
					Client* o = s->m_Clients[j];

					// client id for this client
					buf[0] = o->clientid;

					// put the data
					memcpy(&buf[1], PlayerDataToBuffer(o->data), plen);

					// send the packet and clean up
					s->SendPacket(c->clientid, player_data, buf, plen + 1);
					//sendto(s->m_Socket, (char*)buf, 25, 0, (sockaddr*)&c->sinfo, sizeof(c->sinfo));
					delete[] buf;
				}
			}

			// loop through any messages that need acknowledging and re-send them (doesn't matter if they've been acknowledged
			// and that ack hasn't reached us yet)
			char buf[BUFFER_SIZE];
			for (GLuint j = 0; j < ACK_MSG_COUNT; j++) {
				if (!c->ackmsgs[j].isfree)
				{
					if (--c->ackmsgs[j].count == 0)
					{
						// create the message
						memset(buf, 0, BUFFER_SIZE);
						SetBit(buf[0], 0, 1);
						memcpy(&buf[1], (char*)c->ackmsgs[j].buf, c->ackmsgs[j].len);

						// transmit
						sendto(s->m_Socket, buf, c->ackmsgs[j].len + 1, 0, (sockaddr*)&c->sinfo, sizeof(c->sinfo));

						// reset the counter
						c->ackmsgs[j].count = 6;
					}
				}
			}
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void Server::WorldThread(Server* s)
{
	// this will tick every 50 milliseconds and update anything miscellaneous in the world, ie projectiles, minecarts, wiring
	while (m_Running)
	{
		// local delta time used for this function
		static std::chrono::time_point<std::chrono::system_clock> time = std::chrono::system_clock::now();
		std::chrono::time_point<std::chrono::system_clock> newtime = std::chrono::system_clock::now();
		std::chrono::duration<double> duration = newtime - time;

		double delta = duration.count();
		time = newtime;
		
		// loop through any projectiles and see what needs updating
		s->m_Mutex.lock();
		if (!s->m_Projectiles.empty())
		{
			std::list<Projectile>::iterator it = s->m_Projectiles.begin();
			TerrainManager* t = s->m_pTerrain;
			while (it != s->m_Projectiles.end())
			{
				// get the projectile
				Projectile& p = *it;

				// projectile life
				p.life += 200.f * delta;

				// move this projectile
				if (p.data.gravity > 0)
					p.gravity -= 50.f * delta;

				p.data.position += vector3df(
					p.data.speed * cos(p.data.direction.x) * cos(p.data.direction.y) * delta,
					p.gravity * delta,
					p.data.speed * sin(p.data.direction.x) * cos(p.data.direction.y) * delta
					);
				
				// check if this projectile has hit anything
				const vector<Mob*>* mobs = s->m_pMobMan->GetMobs();
				TerrainBlock block = t->GetBlock(p.data.position);
				bool bHit = block.Solid;

				if (bHit && (block.XX || block.XY || block.YX || block.YY))
				{
					// if this block has any lowered corners, check we've actually collided
					if (p.data.position.y > t->GetHeightBelow(p.data.position))
						bHit = false;
				}

				if (p.life > 1000.f)
					bHit = true;

				if (!bHit)
				{
					if (p.clientid == 255)
					{
						// this projectile is aimed at players, so it must have been created by a mob
						for (GLuint i = 0; i < s->m_MaxClients; i++) {
							if (s->m_Clients[i] && p.data.position.QuickDistance(s->m_Clients[i]->data.pos + vector3df(0, 8, 0)) <= 45.f)
							{
								// this projectile has hit a client, hurt the client and delete the projectile
								s->HitClient(i, p.damage + rand() % (p.damage / 3), p.data.position);

								bHit = true;
								break;
							}
						}
					}
					else
					{
						// this projectile is aimed at mobs / other players (PVP NOT YET IMPLEMENTED)
						for (GLuint i = 0; i < mobs->size(); i++) {
							Mob* mob = mobs->at(i);
							if (mob && p.data.position.QuickDistance(mob->data.pos + vector3df(0, 8, 0)) <= 45.f)
							{
								// get the angle
								vector3df diff = p.data.position - mob->data.pos;
								float angle = atan2(diff.z, diff.x);
								
								if (!s->m_pMobMan->Dodge(mob, angle))
								{
									// this projectile has hit a mob, hurt the mob and delete the projectile
									mob->data.health -= p.damage + rand() % (p.damage / 3);
									mob->hitcount = 5;

									if (p.clientid > -1 && mob->target != p.clientid)
										mob->target = p.clientid;

									// if this is a ranged mob, stop them from shooting for a tiny bit
									if (mob->type->Style == ranged)
										mob->rangecount += 5;

									s->m_pMobMan->KnockBack(i, angle + PI, 10.f * ((float)p.knockback / 100.f));

									bHit = true;
									break;
								}
							}
						}
					}
				}

				if (bHit)
				{
					// there is a chance this projectile will spawn a copy of itself where it crashed
					if (p.clientid < 255 && rand() % 100 < 37)
					{
						const ItemClass* ic = Item::GetItemClass(p.data.index);
						if (ic && ic->Type == AMMO)
						{
							s->CreateItem(ic->strName, p.data.position);
						}
					}
					
					it = s->m_Projectiles.erase(it);
					continue;
				}

				it++;
			}
		}
		s->m_Mutex.unlock();

		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void Server::ServerTickThread(Server* s)
{
	// this will tick every second and send a server packet to all clients with time of day info, weather info etc
	while (m_Running)
	{
		// update the time of day
		// time of day is done on a scale of 0 - 2pi
		s->m_TimeOfDay += (s->m_TimeOfDay > DAY_BEGIN && s->m_TimeOfDay < DAY_END ? 0.0005f : 0.002f);
		//s->m_TimeOfDay = PI_OVER_2;
		if (s->m_TimeOfDay > 2)
			s->m_TimeOfDay = 0;

		// pack these into a buffer to send to all clients
		unsigned char buf[HEADER_SIZE + 5];
		buf[0] = 0;
		buf[1] = server_packet;
		buf[HEADER_SIZE] = sp_server_update;
		FloatToByte(s->m_TimeOfDay, &buf[HEADER_SIZE + 1]);
		SetBit(buf[0], 0, 1);

		// send the data
		s->SendToAllClients((char*)buf, HEADER_SIZE + 5);

		// sleep for one second
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

Client* Server::GetClientFromSockAddrInfo(const sockaddr_in& s)
{
	// get the client from the socket address info struct
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (!m_Clients[i])
			continue;

		if (m_Clients[i]->sinfo.sin_addr.s_addr == s.sin_addr.s_addr &&
			m_Clients[i]->sinfo.sin_port == s.sin_port)
			return m_Clients[i];
	}

	return NULL;
}

Client* Server::GetClientFromID(int id)
{
	// get the client from the ID
	if (id > -1 && id < m_MaxClients)
		return m_Clients[id];
	else
		return NULL;
}

Client* Server::GetClientFromName(const string& name)
{
	// get the client from the name
	for (GLuint i = 0; i < m_MaxClients; i++) {
		if (m_Clients[i] && m_Clients[i]->name == name)
			return m_Clients[i];
	}

	return NULL;
}

bool Server::FindItem(const ItemData& data, GLuint& out)
{
	// see if this item exists
	for (GLuint i = 0; i < m_Items.size(); i++) {
		if (m_Items[i] == data)
		{
			out = i;
			return true;
		}
	}

	return false;
}

void Server::AcknowledgeMessage(const MessageHeader& other, const sockaddr_in& s)
{
	// this message is an acknowledgement
	Client* client = GetClientFromSockAddrInfo(s);
	if (client)
	{
		for (GLuint i = 0; i < ACK_MSG_COUNT; i++) {
			if (client->ackmsgs[i].Compare(other))
			{
				// mark this ack message as free
				client->ackmsgs[i].isfree = true;
				//std::cout << "Client has acknowledged message" << std::endl;
				return;
			}
		}
	}
}

// client control functions
void Server::MoveClient(int client, const vector3df& pos)
{
	unsigned char mbuf[14];
	mbuf[0] = sp_move_player;
	mbuf[1] = client;
	FloatToByte(pos.x, &mbuf[2]);
	FloatToByte(pos.y, &mbuf[6]);
	FloatToByte(pos.z, &mbuf[10]);

	SendPacket(client, server_packet, mbuf, 14, true);
}

void Server::HitClient(int client, short damage, const vector3df& pos)
{
	// ensure this client exists
	if (m_Clients[client])
	{
		// check if we can hit this client
		if (m_Clients[client]->stats.health > 0 && m_Clients[client]->hitcount <= 0)
		{
			// calculate the damage based on the client's defences
			damage = std::max(damage - m_Clients[client]->defence, 1);
			
			// hit this client!
			SetClientHealth(client, std::max(m_Clients[client]->stats.health - damage, 0));
			m_Clients[client]->hitcount = 20;

			// calculate the knockback of this client
			vector3df diff = m_Clients[client]->data.pos - pos;
			GLfloat knockbackdir = atan2(diff.z, diff.x);
			GLfloat knockbackspeed = 1.1f;

			// send the client a knockback message
			unsigned char buf[10];
			buf[0] = sp_knockback;
			buf[1] = client;
			FloatToByte(knockbackdir, &buf[2]);
			FloatToByte(knockbackspeed, &buf[6]);

			SendPacket(client, server_packet, buf, 10, true);
		}
	}
}

void Server::SetClientHealth(int client, unsigned short health)
{
	// ensure this client exists
	if (m_Clients[client])
	{
		// set the health
		Stats& stats = m_Clients[client]->stats;
		stats.health = health;

		// send a packet to all clients with this new information
		SendStats(m_Clients[client]);
	}
}

void Server::SetClientEnergy(int client, unsigned short energy)
{
	// ensure this client exists
	if (m_Clients[client])
	{
		// set the energy
		Stats& stats = m_Clients[client]->stats;
		stats.energy = energy;

		// send a packet to all clients with this new information
		SendStats(m_Clients[client]);
	}
}

void Server::SendStats(Client* c)
{
	// send a packet to all clients with the stats
	const size_t len = sizeof(Stats) + 1;
	unsigned char buf[len];
	buf[0] = c->clientid;
	memcpy(&buf[1], Stats::ToBuffer(c->stats), len - 1);

	SendPacketToAllClients(player_stat_data, buf, len, true);
}

void Server::CreateProjectile(string name, vector3df pos, vector2df rotation, short damage, short knockback, Client* c)
{
	const ItemClass* ammo = Item::GetItemClass(name);
	if (ammo)
	{
		CreateProjectile(ammo, pos, rotation, damage, knockback, c);
	}
}

void Server::CreateProjectile(const ItemClass* ammo, vector3df pos, vector2df rotation, short damage, short knockback, Client* c)
{
	// the ammo type exists and is loaded onto the server, let us create one
	ProjectileData data;
	data.direction.x = -rotation.y;
	data.direction.y = rotation.x + (0.1f * ammo->sValues[7]);
	data.speed = ammo->fValues[0];
	data.gravity = ammo->sValues[7];
	data.position = pos;
	data.index = ammo->index;

	// create a message to send to all clients
	const size_t plen = sizeof(ProjectileData);
	unsigned char* buf = new unsigned char[plen + 1];
	buf[0] = (c ? c->clientid : 255);
	memcpy(&buf[1], ProjectileDataToBuffer(data), plen);

	// send the message and clean up
	SendPacketToAllClients(new_projectile, buf, plen + 1);
	delete[] buf;

	// create a new projectile in the world we will update and use to check for collisions
	Projectile p;
	p.data = data;
	p.damage = damage;
	p.knockback = knockback;
	p.clientid = (c ? c->clientid : 255);
	p.gravity = ammo->fValues[0] * sin(data.direction.y);
	p.life = 0.f;
	AddProjectile(p);
}

void Server::CreateProjectile(string name, vector3df pos, vector3df target, short damage, short knockback, Client* c)
{
	const ItemClass* ammo = Item::GetItemClass(name);
	if (ammo)
	{
		CreateProjectile(ammo, pos, target, damage, knockback, c);
	}
}

void Server::CreateProjectile(const ItemClass* ammo, vector3df pos, vector3df target, short damage, short knockback, Client* c)
{
	// calculate the angles for the projectiles - it will be shot towards the block the player is aiming at
	vector3df one = pos;
	vector3df two = target;
	float dist = vector2df(one.x, one.z).Distance(vector2df(two.x, two.z));
	float angle1 = atan2(one.z - two.z, one.x - two.x);
	float angle2 = atan2(one.y - two.y, dist);
	dist = one.Distance(two) + (two.y - one.y);

	// the ammo type exists and is loaded onto the server, let us create one
	ProjectileData data;
	data.direction.x = angle1 + PI;
	data.direction.y = -angle2 + (std::max(std::min(dist * 0.0035f, 0.8f), 0.085f) * ammo->sValues[7]);
	data.speed = ammo->fValues[0];
	data.gravity = ammo->sValues[7];
	data.position = pos;
	data.index = ammo->index;

	// create a message to send to all clients
	const size_t plen = sizeof(ProjectileData);
	unsigned char* buf = new unsigned char[plen + 1];
	buf[0] = (c ? c->clientid : 255);
	memcpy(&buf[1], ProjectileDataToBuffer(data), plen);

	// send the message and clean up
	SendPacketToAllClients(new_projectile, buf, plen + 1);
	delete[] buf;

	// create a new projectile in the world we will update and use to check for collisions
	Projectile p;
	p.data = data;
	p.damage = damage;
	p.knockback = knockback;
	p.clientid = (c ? c->clientid : 255);
	p.gravity = ammo->fValues[0] * sin(data.direction.y);
	AddProjectile(p);
}

void Server::ClientUsingItem(Client* c, double delta)
{
	// if somehow we are here erroneously, stop anything bad from happening
	if (c->data.selected >= c->hotbar.items.size() || c->data.selected < 0)
		return;
	
	// a client is using an item, see if we as the server need to do anything
	const ItemClass* ic = c->hotbar.items.at(c->data.selected).ic;

	// if the client is holding nothing, we don't need to do anything
	if (!ic)
		return;

	// is the client attacking with a melee weapon?
	if (ic->Type == WEAPON)
	{
		if (ic->TypeSub == MELEE)
			m_pMobMan->ClientMeleeAttacking(c);
		else if (ic->TypeSub == RANGED)
		{
			// the client is firing a ranged weapon (maybe)
			// add to attack until we reach firing point
			c->attack += ic->fValues[0] * delta;

			if (c->attack >= 1.f)
			{
				// check to see if we have any ammo in our client's equipment
				InventoryItem* item = FindItem(ItemFilter(AMMO, ic->AmmoType), &c->equipment.items);
				if (item && item->amount > 0)
				{
					// it's time to fire a projectile! figure out trajectory and whatnot based on client's position, rotation and the itemclass for the ammo used
					CreateProjectile(item->name, c->data.pos + vector3df(0, 14, 0), c->data.rot, ic->sValues[1], ic->sValues[6], c);
					--item->amount;
				}

				c->attack = 0.f;
			}
		}
		else if (ic->TypeSub == MAGIC)
		{
			// the client is firing a magic weapon, ensure we have enough mana to do so
			if (c->stats.energy >= ic->sValues[2])
			{
				// add to attack until we reach firing point
				c->attack += ic->fValues[0] * delta;

				if (c->attack >= 1.f)
				{
					// it's time to fire a projectile! figure out trajectory and whatnot based on client's position, rotation and the itemclass for the ammo used
					CreateProjectile(ic->strProjectileType, c->data.pos + vector3df(0, 14, 0), c->data.rot, ic->sValues[1], ic->sValues[6], c);
					SetClientEnergy(c->clientid, c->stats.energy - ic->sValues[2]);

					c->attack = 0.f;
				}
			}
		}
	}
}

void Server::AddProjectile(Projectile p)
{
	// add a new projectile to our list
	m_Mutex.lock();

	if (p.data.gravity > 0)
		p.gravity = p.data.speed * sin(p.data.direction.y);

	m_Projectiles.push_back(p);
	m_Mutex.unlock();
}

bool Server::FilterCheck(const ItemClass* ic, const ItemFilter& filter)
{
	// check if the given item class passes our filter
	if (!ic)
		return false;

	if (filter.type_filter != NO_TYPE && ic->Type != filter.type_filter)
		return false;

	if (filter.subtype_filter != NO_SUBTYPE && ic->TypeSub != filter.subtype_filter)
		return false;

	return true;
}

InventoryItem* Server::FindItem(const ItemFilter& filter, vector<InventoryItem>* inv)
{
	// find the first item which passes the given filter
	for (GLuint i = 0; i < inv->size(); i++) {
		if (FilterCheck(inv->at(i).ic, filter))
			return &inv->at(i);
	}

	return NULL;
}

void Server::ProcessClientAction(Client* c, int type)
{
	// process the given action the client is saying they've done
	switch (type)
	{
		case player_rolled:
		{
			// the player has rolled in a direction, decrease their energy and let everyone else know
			SetClientEnergy(c->clientid, c->stats.energy - 7);

			break;
		}
	}
}
