# Pyre of Gods Server

This is the server application for my old game project Pyre of Gods (see the relevant repo).

**Note the code isn't the neatest or best designed**

I wrote this ~4-5 years ago when I was teaching myself OpenGL, networking and threading in C++, so things might be a bit messy.


**To compile**

I have this compiling under Pop!_OS 20.04 (so Ubuntu 20.04), but the original work was done under Windows in VS2015.

The dependencies are in the makefile but for the server you'll need:
- zlib
- pthread

Everything is in the makefile, note it's not a very good makefile so any changes to headers will need a clean compile. Just run

> make

to compile release and

> make debug

to compile debug.

> make clean

will clean everything.
